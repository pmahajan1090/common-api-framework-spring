package net.bglgroup.spring.common.apiframework.testclasses;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.ControllerAdvice;

import net.bglgroup.spring.common.apiframework.errorhandler.BaseControllerAdvice;
import net.bglgroup.spring.common.apientry.loggers.Logger;

@ControllerAdvice
public class TestControllerAdvice extends BaseControllerAdvice
{		
	public void setHttpLogger(Logger<HttpServletRequest, HttpServletResponse> httpLogger)
	{
		this.httpLogger = httpLogger;
	}
}
