package net.bglgroup.spring.common.apiframework.errorhandler;

import static org.junit.jupiter.api.Assertions.assertEquals;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;


import org.mockito.Mock;
import org.springframework.http.HttpStatus;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.client.UnknownHttpStatusCodeException;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import net.bglgroup.spring.common.apiframework.testclasses.TestController;
import net.bglgroup.spring.common.apiframework.testclasses.TestControllerAdvice;
import net.bglgroup.spring.common.apientry.loggers.Logger;

@ExtendWith(SpringExtension.class)
public class BaseControllerAdviceTest 
{		
	@Mock
	private Logger<HttpServletRequest, HttpServletResponse> httpSystemLogger;
			
	private MockMvc mvc;
	 	
	@BeforeEach
	public void initObjects()
	{				
		TestControllerAdvice testControllerAdvice = new TestControllerAdvice();
		
		testControllerAdvice.setHttpLogger(httpSystemLogger);
		
		mvc = MockMvcBuilders.standaloneSetup(new TestController())
				             .setControllerAdvice(testControllerAdvice).build();
    }
	
	@Test
	public void runtimeExceptionTest() throws Exception
    {
		MockHttpServletResponse response = mvc.perform( MockMvcRequestBuilders.get("/runtimeexceptiontest")).andReturn().getResponse();
		
		assertEquals(HttpStatus.INTERNAL_SERVER_ERROR.value(), response.getStatus());
		assertEquals("runtime exception", response.getContentAsString());	
    }
	
	@Test
	public void apiExceptionTest() throws Exception
    {
		MockHttpServletResponse response = mvc.perform( MockMvcRequestBuilders.get("/apiexceptiontest")).andReturn().getResponse();
		
		assertEquals(HttpStatus.INTERNAL_SERVER_ERROR.value(), response.getStatus());
		assertEquals("api exception", response.getContentAsString());	
    }
	
	@Test
	public void badRequestExceptionTest() throws Exception
    {
		MockHttpServletResponse response = mvc.perform( MockMvcRequestBuilders.get("/badrequestexceptiontest")).andReturn().getResponse();
		
		assertEquals(HttpStatus.BAD_REQUEST.value(), response.getStatus());
		assertEquals("bad request exception", response.getContentAsString());	
    }
		
	@Test
	public void notFoundTest() throws Exception
    {
		MockHttpServletResponse response = mvc.perform( MockMvcRequestBuilders.get("/notfound")).andReturn().getResponse();
		
		assertEquals(HttpStatus.NOT_FOUND.value(), response.getStatus());	
    }
	
	@Test
	public void webClientRequestExceptionTest() throws Exception
    {
		MockHttpServletResponse response = mvc.perform( MockMvcRequestBuilders.get("/webclientrequestexceptiontest")).andReturn().getResponse();
		
		assertEquals(HttpStatus.INTERNAL_SERVER_ERROR.value(), response.getStatus());
    }
	
	@Test
	public void webClientResponseExceptionTest() throws Exception
    {
		MockHttpServletResponse response = mvc.perform( MockMvcRequestBuilders.get("/webclientresponseexceptiontest")).andReturn().getResponse();
		
		assertEquals(HttpStatus.BAD_REQUEST.value(), response.getStatus());
    }
	
	@Test
	public void httpClientErrorExceptionTest() throws Exception
    {
		MockHttpServletResponse response = mvc.perform( MockMvcRequestBuilders.get("/httpclienterrorexceptiontest")).andReturn().getResponse();
		
		assertEquals(HttpStatus.BAD_REQUEST.value(), response.getStatus());
    }
	
	@Test
	public void httpServerErrorExceptionTest() throws Exception
    {
		MockHttpServletResponse response = mvc.perform( MockMvcRequestBuilders.get("/httpservererrorexceptiontest")).andReturn().getResponse();
		
		assertEquals(HttpStatus.INTERNAL_SERVER_ERROR.value(), response.getStatus());
    }
	
	@Test
	public void unknownHttpStatusCodeExceptionTest() throws Exception
    {
		MockHttpServletResponse response = mvc.perform( MockMvcRequestBuilders.get("/unknownhttpstatuscodeexceptiontest")).andReturn().getResponse();
		
		assertEquals(HttpStatus.INTERNAL_SERVER_ERROR.value(), response.getStatus());
    }
	
	@Test
	public void invalidRequestExceptionTest() throws Exception
    {
		MockHttpServletResponse response = mvc.perform( MockMvcRequestBuilders.get("/invalidrequestexceptiontest")).andReturn().getResponse();
		
		assertEquals(HttpStatus.BAD_REQUEST.value(), response.getStatus());
		assertEquals("OpenApi Error", response.getContentAsString());	
    }
	
	@Test
	public void invalidResponseExceptionTest() throws Exception
    {
		MockHttpServletResponse response = mvc.perform( MockMvcRequestBuilders.get("/invalidresponseexceptiontest")).andReturn().getResponse();
		
		assertEquals(HttpStatus.BAD_REQUEST.value(), response.getStatus());
		assertEquals("OpenApi Error", response.getContentAsString());	
    }
	
	
}

