package net.bglgroup.spring.common.apiframework.webclientrequests;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClient.RequestBodySpec;
import org.springframework.web.reactive.function.client.WebClient.RequestBodyUriSpec;
import org.springframework.web.reactive.function.client.WebClient.RequestHeadersSpec;
import org.springframework.web.reactive.function.client.WebClient.RequestHeadersUriSpec;
import org.springframework.web.reactive.function.client.WebClient.ResponseSpec;

import net.bglgroup.spring.common.apiframework.testclasses.TestObject;
import reactor.core.publisher.Mono;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = {WebClientCalls.class})
public class WebClientCallsTest 
{		
	@MockBean
	private RestTemplate restTemplate;
	    
	@MockBean
	private WebClient webClient;
	
	@MockBean
	private WebClientLogger<WebClientDetails<?,?>, String> webClientLogger;
		
	@Autowired
	private WebClientCalls webClientCalls;
	
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Test
	public void getWebClientCallParallelTest()
    {
        Mono<String> mono = Mockito.mock(Mono.class);
		
		RequestHeadersUriSpec uriSpecMock      = Mockito.mock(WebClient.RequestHeadersUriSpec.class);
		RequestHeadersSpec    headersSpecMock  = Mockito.mock(WebClient.RequestHeadersSpec.class);
		ResponseSpec          responseSpecMock = Mockito.mock(WebClient.ResponseSpec.class);
		
		assertDoesNotThrow(() ->
		{					
			WebClientDetails<?,?>  webClientDetails = new WebClientDetails<Void, String>("testendpoint", 
					  																	 HttpMethod.GET, 
					  																	 null,
					  																	 Void.class,
					  																	 new HttpHeaders(),
					  																	 String.class,
					  																	 Long.valueOf(10));
						
			when(webClient.get()).thenReturn(uriSpecMock);
			when(uriSpecMock.uri(ArgumentMatchers.anyString())).thenReturn(headersSpecMock);
			when(headersSpecMock.headers(ArgumentMatchers.any())).thenReturn(headersSpecMock);
			when(headersSpecMock.retrieve()).thenReturn(responseSpecMock);
			
			
			when(responseSpecMock.bodyToMono(ArgumentMatchers.<Class<String>>any())).thenReturn(mono);
			when(mono.block()).thenReturn("1");
			
			
			ArrayList<WebClientDetails<?,?>> webClientDetailsList = new ArrayList<WebClientDetails<?,?>>();
			webClientDetailsList.add(webClientDetails);
			
			ArrayList<?> responseArrayList = webClientCalls.webClientParallelCalls(webClientDetailsList);
			
			assertEquals(responseArrayList.get(0), "1");
		});

	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Test
	public void getWebClientCallParallelArrayTest()
    {
        Mono<ArrayList<String>> monoArray = Mockito.mock(Mono.class);
		
		RequestHeadersUriSpec uriSpecMock      = Mockito.mock(WebClient.RequestHeadersUriSpec.class);
		RequestHeadersSpec    headersSpecMock  = Mockito.mock(WebClient.RequestHeadersSpec.class);
		ResponseSpec          responseSpecMock = Mockito.mock(WebClient.ResponseSpec.class);
		
		assertDoesNotThrow(() ->
		{								
			WebClientDetails<?,?>  webClientDetailsArray = new WebClientDetails<Void, String>("testendpoint", 
																							 HttpMethod.GET, 
																							 null,
																							 Void.class,
																							 new HttpHeaders(),
																							 new ParameterizedTypeReference<ArrayList<String>>() {},
																							 Long.valueOf(10));
			
			when(webClient.get()).thenReturn(uriSpecMock);
			when(uriSpecMock.uri(ArgumentMatchers.anyString())).thenReturn(headersSpecMock);
			when(headersSpecMock.headers(ArgumentMatchers.any())).thenReturn(headersSpecMock);
			when(headersSpecMock.retrieve()).thenReturn(responseSpecMock);
			
						
			ArrayList<String> testArrayList = new ArrayList<String>();
			testArrayList.add("2");
			
			when(responseSpecMock.bodyToMono(ArgumentMatchers.<ParameterizedTypeReference<ArrayList<String>>>any())).thenReturn(monoArray);
			when(monoArray.block()).thenReturn(testArrayList);
			
			ArrayList<WebClientDetails<?,?>> webClientDetailsList = new ArrayList<WebClientDetails<?,?>>();
			webClientDetailsList.add(webClientDetailsArray);
			
			ArrayList<?> responseArrayList = webClientCalls.webClientParallelCalls(webClientDetailsList);
			
			assertEquals(((ArrayList)responseArrayList.get(0)).get(0), "2");
		});

	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Test
	public void getWebClientCallTest()
    {		
		Mono<String> mono  = Mockito.mock(Mono.class);
		
		RequestHeadersUriSpec uriSpecMock      = Mockito.mock(WebClient.RequestHeadersUriSpec.class);
		RequestHeadersSpec    headersSpecMock  = Mockito.mock(WebClient.RequestHeadersSpec.class);
		ResponseSpec          responseSpecMock = Mockito.mock(WebClient.ResponseSpec.class);
		
				
		assertDoesNotThrow(() ->
		{							
			var webClientDetails = new WebClientDetails<Void, String>("testendpoint", 
                    					                              HttpMethod.GET, 
                    					                              null,
                    					                              Void.class,
                    					                              new HttpHeaders(),
                    					                              String.class,
                    					                              Long.valueOf(10));

			
			//Mock out each method of the WebClient Call
			when(webClient.get()).thenReturn(uriSpecMock);
			when(uriSpecMock.uri(ArgumentMatchers.anyString())).thenReturn(headersSpecMock);
			when(headersSpecMock.headers(ArgumentMatchers.any())).thenReturn(headersSpecMock);
			when(headersSpecMock.retrieve()).thenReturn(responseSpecMock);
			when(responseSpecMock.bodyToMono(webClientDetails.getResponseClass().get())).thenReturn(mono);
			when(mono.block()).thenReturn("test string");
			
			
			String response = (String) webClientCalls.webClientCall(webClientDetails);
			
			assertEquals(response, "test string");
			
		});
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Test
	public void patchWebClientCallTest()
    {		
		Mono<Void> mono  = Mockito.mock(Mono.class);
		
		RequestHeadersSpec    headersSpecMock  = Mockito.mock(WebClient.RequestHeadersSpec.class);
		RequestBodyUriSpec    requestBodyUriSpecMock = Mockito.mock(WebClient.RequestBodyUriSpec.class);
		RequestBodySpec       requestBodySpecMock = Mockito.mock(WebClient.RequestBodySpec.class);
		ResponseSpec          responseSpecMock = Mockito.mock(WebClient.ResponseSpec.class);
		
				
		assertDoesNotThrow(() ->
		{							
			var webClientDetails = new WebClientDetails<TestObject, Void>("testendpoint", 
                    					                                      HttpMethod.PATCH, 
                    					                                      "{ \"field\":\"test\" }",
                    					                                      TestObject.class,
                    					                                      new HttpHeaders(),
                    					                                      Void.class,
                    					                                      Long.valueOf(10));

						
			//Mock out each method of the WebClient Call
			when(webClient.patch()).thenReturn(requestBodyUriSpecMock);
			when(requestBodyUriSpecMock.uri(ArgumentMatchers.anyString())).thenReturn(requestBodySpecMock);
			when(requestBodySpecMock.headers(ArgumentMatchers.any())).thenReturn(requestBodySpecMock);
		    when(requestBodySpecMock.contentType(ArgumentMatchers.any())).thenReturn(requestBodySpecMock);
			when(requestBodySpecMock.bodyValue(ArgumentMatchers.any())).thenReturn(headersSpecMock);			
			when(headersSpecMock.retrieve()).thenReturn(responseSpecMock);
			when(responseSpecMock.bodyToMono(ArgumentMatchers.<Class<Void>>any())).thenReturn(mono);
			when(mono.block()).thenReturn(null);	
			 			
			
			webClientCalls.webClientCall(webClientDetails);
			
		});
	}
}
