package net.bglgroup.spring.common.apiframework.testclasses;

import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@NoArgsConstructor
public class TestObject 
{
	String field="test";
}
