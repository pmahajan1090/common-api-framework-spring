package net.bglgroup.spring.common.apiframework.model;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
public class ActionTest 
{	
	@Test
	public void actionTest()
	{		
		 ArrayList<String> fields = new ArrayList<String>();
		 fields.add("line1");
		    
		 Action action = new Action("Update Addresses", "Addresses", "PATCH",  fields);
		 		 
		 assertEquals(action.getMethod(), "PATCH");
		 assertEquals(action.getTitle(), "Addresses");
		 assertEquals(action.getName(), "Update Addresses");
		 assertTrue(action.getFields().contains("line1"));

	}
}