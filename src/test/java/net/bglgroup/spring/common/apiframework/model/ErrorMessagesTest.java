package net.bglgroup.spring.common.apiframework.model;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import net.bglgroup.spring.common.apiframework.exceptions.ApiException;

import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
public class ErrorMessagesTest 
{	
	@Test
	public void errorResponseTest()
	{		
		 ErrorResponse errorResponse = new ErrorResponse("123", "error");
		
		 ArrayList<ErrorResponse> errorMessagesList = new ArrayList<ErrorResponse>();
		 errorMessagesList.add(errorResponse);
		    
		 ErrorMessages errorMessages = new ErrorMessages(errorMessagesList);
		 
		 ApiException apiException = new ApiException(HttpStatus.BAD_REQUEST, "Bad Request", errorMessages);
		 
		 assertTrue(apiException.getHttpStatus().equals(HttpStatus.BAD_REQUEST));
		 assertTrue(apiException.getMessage().equals("Bad Request"));
		 
		 assertTrue(apiException.getErrorMessages().getMessages().size() == 1);
		 assertTrue(apiException.getErrorMessages().getMessages().get(0).getCode().equals("123"));
		 assertTrue(apiException.getErrorMessages().getMessages().get(0).getDescription().equals("error"));
	}
}