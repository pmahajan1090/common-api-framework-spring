package net.bglgroup.spring.common.apiframework.webclientrequests;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.text.MessageFormat;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Spy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import net.bglgroup.spring.common.apientry.enums.StandardHeadersEnum;
import net.bglgroup.spring.common.apientry.loggers.Logger;
import net.bglgroup.spring.common.apiframework.testclasses.TestObject;

@ExtendWith(SpringExtension.class)
public class WebClientLoggerTest 
{

	@Autowired
	private Logger<WebClientDetails<?, ?>, String> webClientLogger;

	private final String expectedStartLog = "IO=Inbound, HM=GET, URI=/system/v1/addresses, IT=X-Trace-Id, IR=X-Request-Id, IM=X-Message-Id, IB=Basket-Id, E=system";
	private final String expectedEndLog = "IO=Outbound, HM=GET, URI=/system/v1/addresses, IT=X-Trace-Id, IR=X-Request-Id, IM=X-Message-Id, IB=Basket-Id, E=system, RC=200, RT=10";
	private final String expectedEndLogExc = "IO=Outbound, HM=GET, URI=/system/v1/addresses, IT=X-Trace-Id, IR=X-Request-Id, IM=X-Message-Id, IB=Basket-Id, E=system, RC=200, RT=10, Exception=Test Exception";
	
	@Spy
	private WebClientDetails<Void, TestObject> webClientDetails;
	
	@TestConfiguration
	static class HttpLoggerTestContextConfiguration 
	{
		@Bean
		public Logger<WebClientDetails<Void, TestObject>, String> webClientLogger() 
		{
			return new WebClientLogger<WebClientDetails<Void, TestObject>, String>("system");
		}
	}

	@BeforeEach
    public void initObjects() throws Throwable
	{
		var headers = new HttpHeaders();
		headers.add(StandardHeadersEnum.TRACE_ID.getName(), StandardHeadersEnum.TRACE_ID.getName());
		headers.add(StandardHeadersEnum.REQUEST_ID.getName(), StandardHeadersEnum.REQUEST_ID.getName());
		headers.add(StandardHeadersEnum.MESSAGE_ID.getName(), StandardHeadersEnum.MESSAGE_ID.getName());
		headers.add(StandardHeadersEnum.BASKET_ID.getName(), StandardHeadersEnum.BASKET_ID.getName());
		
		when(webClientDetails.generateGuid()).thenReturn(StandardHeadersEnum.MESSAGE_ID.getName());
		
		when(webClientDetails.getUrl()).thenReturn("/system/v1/addresses");
		when(webClientDetails.getMethod()).thenReturn(HttpMethod.GET);
		when(webClientDetails.getHeaders()).thenReturn(headers);
		when(webClientDetails.getRequestStartTime()).thenReturn(Long.valueOf(10));
		
	}
	
	@Test
	public void webClientStartLoggerTest() 
	{
		String outputMessage = webClientLogger.printStartLogStatement(webClientDetails);
		
		assertEquals(expectedStartLog, outputMessage);
	}
	
	@Test
	public void webClientEndLoggerTest() 
	{		
		String outputMessage = webClientLogger.printEndLogStatement(webClientDetails, String.valueOf(HttpStatus.OK.value()), Long.valueOf(20));
		
		assertEquals(expectedEndLog, outputMessage);
	}
	
	@Test
	public void webClientExceptionEndLoggerTest() 
	{				
		String outputMessage = webClientLogger.printEndLogStatement("Test Exception", webClientDetails, String.valueOf(HttpStatus.OK.value()), Long.valueOf(20));

		assertEquals(expectedEndLogExc, outputMessage);
	}
}
