package net.bglgroup.spring.common.apiframework.requests;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.BufferedReader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.http.HttpMethod;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import net.bglgroup.spring.common.apientry.enums.ApiRequestEnum;
import net.bglgroup.spring.common.apientry.enums.StandardHeadersEnum;


@ExtendWith(SpringExtension.class)
public class ApiJsonRequestTest 
{	
	@Mock
	private HttpServletRequest  mockedRequest;	
	
	@Test
	public void apiJsonRequestTest()
    {				
		assertDoesNotThrow(()-> {			
			
			// Build Mock Request
			
            HttpServletRequest request = Mockito.mock(HttpServletRequest.class);
			
			String requestPayloadString = "{ testField1:testValue1, testField2:testValue2, testField3:testValue3, testField4:testValue4 }";
				
			Mockito.when(request.getMethod()).thenReturn(HttpMethod.PATCH.name());
			Mockito.when(request.getRequestURI()).thenReturn("host:port/api/v1/resource");
			Mockito.when(request.getContentType()).thenReturn("Application/Json");	
			Mockito.when(request.getAttribute(ApiRequestEnum.START_TIME.getName())).thenReturn(Long.valueOf(0));
							
			List<String> headerlist = new ArrayList<String>();
			for (StandardHeadersEnum header : StandardHeadersEnum.values())
			{
				headerlist.add(header.getName());
				Mockito.when(request.getHeader(header.getName())).thenReturn(header.getName());
			}
			Mockito.when(request.getHeaderNames()).thenReturn(Collections.enumeration(headerlist));		
			Mockito.when(request.getReader()).thenReturn(new BufferedReader(new StringReader(requestPayloadString)));
			
			String[] includes = new String[]{"resource1, resource2"};			
			Map<String, String[]> queryParameters = new HashMap<String, String[]>();
			queryParameters.put("queryParam1", new String[]{"queryParamValue1"});
			queryParameters.put("include", includes);
			
			Mockito.when(request.getParameterMap()).thenReturn(queryParameters);
					
					
			// Create the API request using the mocks and schema
			
			AbstractApiRequest apiJsonRequest = new ApiJsonRequest(request, StandardHeadersEnum.values());
			
			
			//Assertions   
			
			assertEquals(apiJsonRequest.getUrl(), "host:port/api/v1/resource");
			assertEquals(apiJsonRequest.getMethod(), "PATCH");
			assertEquals(apiJsonRequest.getContentType(), "Application/Json");
			
			for (StandardHeadersEnum header : StandardHeadersEnum.values())
			{
				assertEquals(apiJsonRequest.getHeader(header.getName()), header.getName());
			}

			assertEquals(apiJsonRequest.getQueryParemeter("queryParam1"), "queryParamValue1");
			assertTrue(apiJsonRequest.includeExists("resource1"));
			assertTrue(apiJsonRequest.includeExists("resource2"));
			
			assertTrue(apiJsonRequest.hasIncludes());
			assertTrue(!apiJsonRequest.actionsRequested());			
			assertTrue(apiJsonRequest.isBasketContext());
					
			assertEquals(apiJsonRequest.rebuildInludeString(new ArrayList<String>(Arrays.asList("resource1","resource2"))), "include=resource1,resource2");
						
		});
		
	}

}
