package net.bglgroup.spring.common.apiframework.requests;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import net.bglgroup.spring.common.apientry.enums.QueueHeadersEnum;
import net.bglgroup.spring.common.apiframework.queuerequests.QueueJsonRequest;


@ExtendWith(SpringExtension.class)
public class QueueJsonRequestTest 
{		
	@Test
	public void apiJsonRequestTest()
    {				
		assertDoesNotThrow(()-> 
		{			
					
			String inputQueueMessage = "{\"testField1\":\"testValue1\"}";
																				
					
			// Create the Queue request			
			QueueJsonRequest queueJsonRequest = new QueueJsonRequest(inputQueueMessage, QueueHeadersEnum.values());
			
			
			//Assertions   			
			for (QueueHeadersEnum header : QueueHeadersEnum.values())
			{
				assertTrue(queueJsonRequest.getHeaders().getFirst(header.getName()) != null);
			}
			
			assertEquals(queueJsonRequest.getPayload().toString(), inputQueueMessage);
		
		});
		
	}

}
