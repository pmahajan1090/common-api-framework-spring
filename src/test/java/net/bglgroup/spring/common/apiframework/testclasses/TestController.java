package net.bglgroup.spring.common.apiframework.testclasses;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.UnknownHttpStatusCodeException;
import org.springframework.web.reactive.function.client.WebClientRequestException;
import org.springframework.web.reactive.function.client.WebClientResponseException;

import com.atlassian.oai.validator.report.ValidationReport;
import com.atlassian.oai.validator.report.ValidationReport.Level;
import com.atlassian.oai.validator.report.ValidationReport.Message;
import com.atlassian.oai.validator.report.ValidationReport.MessageContext;
import com.atlassian.oai.validator.springmvc.InvalidRequestException;
import com.atlassian.oai.validator.springmvc.InvalidResponseException;

import net.bglgroup.spring.common.apiframework.exceptions.ApiException;
import net.bglgroup.spring.common.apiframework.exceptions.BadRequestException;

@RestController
public class TestController 
{        
    @GetMapping(value = "/runtimeexceptiontest", produces = "application/json")
    public String runtimeExceptiontest(HttpServletRequest request)
    {      	    	    	
    	throw new RuntimeException("runtime exception");
    }
    
    @GetMapping(value = "/apiexceptiontest", produces = "application/json")
    public String apiExceptiontest(HttpServletRequest request) throws ApiException
    {      	    	    	
    	throw new ApiException(HttpStatus.INTERNAL_SERVER_ERROR, "api exception");
    }    
    
    @GetMapping(value = "/badrequestexceptiontest", produces = "application/json")
    public String badRequestExceptiontest(HttpServletRequest request) throws BadRequestException
    {      	    	    	
    	throw new BadRequestException("bad request exception");
    } 
    
    @GetMapping(value = "/webclientrequestexceptiontest", produces = "application/json")
    public String webClientRequestExceptionTest(HttpServletRequest request) throws WebClientRequestException
    {      	    	    	
    	throw new WebClientRequestException(new RuntimeException(), null, null, null);
    } 
    
    @GetMapping(value = "/webclientresponseexceptiontest", produces = "application/json")
    public String webClientResponseExceptionTest(HttpServletRequest request) throws WebClientResponseException
    {      	    	    	
    	throw new WebClientResponseException(HttpStatus.BAD_REQUEST.value(), "Web Client Response Error", null, null, null);
    } 
    
    @GetMapping(value = "/httpclienterrorexceptiontest", produces = "application/json")
    public String httpClientErrorExceptionTest(HttpServletRequest request) throws HttpClientErrorException
    {      	    	    	
    	throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Rest Template Client Error");
    } 
    
    @GetMapping(value = "/httpservererrorexceptiontest", produces = "application/json")
    public String httpServerErrorExceptionTest(HttpServletRequest request) throws HttpServerErrorException
    {      	    	    	
    	throw new HttpServerErrorException(HttpStatus.INTERNAL_SERVER_ERROR, "Rest Template Server Error");
    } 
    
    @GetMapping(value = "/unknownhttpstatuscodeexceptiontest", produces = "application/json")
    public String unknownHttpStatusCodeExceptionTest(HttpServletRequest request) throws UnknownHttpStatusCodeException
    {      	    	    	
    	throw new UnknownHttpStatusCodeException(HttpStatus.INTERNAL_SERVER_ERROR.value(), null, null, null, null);
    } 
    
    @GetMapping(value = "/invalidrequestexceptiontest", produces = "application/json")
    public String invalidRequestExceptionTest(HttpServletRequest request) throws InvalidRequestException
    {      	    	    	
    	throw new InvalidRequestException(new Report());
    } 
    
    @GetMapping(value = "/invalidresponseexceptiontest", produces = "application/json")
    public String invalidResponseExceptionTest(HttpServletRequest request) throws InvalidResponseException
    {      	    	    	
    	throw new InvalidResponseException(new Report());
    } 
  
    private class Report implements ValidationReport
    {

		@Override
		public List<Message> getMessages() 
		{
			var messages = new ArrayList<Message>();
			messages.add(new msg());
			return messages;
		}

		@Override
		public ValidationReport withAdditionalContext(MessageContext context) 
		{
			return null;
		}
    	
    }
    
    private class msg implements Message
    {

		@Override
		public String getKey() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public String getMessage() 
		{			
			return "OpenApi Error";
		}

		@Override
		public Level getLevel() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public List<String> getAdditionalInfo() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public Optional<MessageContext> getContext() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public Message withLevel(Level level) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public Message withAdditionalInfo(String info) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public Message withAdditionalContext(MessageContext context) {
			// TODO Auto-generated method stub
			return null;
		}
    	
    }
    
}
