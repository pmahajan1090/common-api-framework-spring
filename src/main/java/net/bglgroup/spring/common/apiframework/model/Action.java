package net.bglgroup.spring.common.apiframework.model;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;


@NoArgsConstructor
@Getter
@Setter
@ToString
public class Action 
{   
	private String name;
    private String title;
    private String method;
    
    private List<String> fields;
     
    public Action(String name, String title, String method, ArrayList<String> fields)
    {
    	this(name, title, method);
    	this.setFields(fields);
    }
    
    public Action(String name, String title, String method)
    {
    	this.setName(name);
    	this.setTitle(title);
    	this.setMethod(method);
    }
    
}
