package net.bglgroup.spring.common.apiframework.webclientrequests;

import java.util.ArrayList;
import java.util.Optional;
import java.util.UUID;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;

import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

@NoArgsConstructor
@Getter
@ToString
public class WebClientDetails <RequestClass, ResponseClass>
{
	private String url;
	private HttpMethod method;
	private HttpHeaders headers;
	
	private Object    requestBody;		
	private Class<RequestClass> requestClass;
	
	private Optional<Class<ResponseClass>> responseClass = Optional.empty();
	private Optional<ParameterizedTypeReference<ArrayList<ResponseClass>>> responseClassArray = Optional.empty();
	private boolean isArrayResponse;
	           
	private Long requestStartTime;
		
	public WebClientDetails(String url, HttpMethod method, Object requestBody,  Class<RequestClass> requestClass, HttpHeaders headers, Class<ResponseClass> responseClass, Long requestStartTime) throws Throwable
	{
		this(url, method, requestBody, requestClass, headers, requestStartTime);
		this.isArrayResponse = false;
		this.responseClass = Optional.ofNullable(responseClass);
	}
	
	public WebClientDetails(String url, HttpMethod method, Object requestBody,  Class<RequestClass> requestClass, HttpHeaders headers, ParameterizedTypeReference<ArrayList<ResponseClass>> responseClassArray, Long requestStartTime) throws Throwable
	{
		this(url, method, requestBody, requestClass, headers, requestStartTime);
		this.isArrayResponse = true;
		this.responseClassArray = Optional.ofNullable(responseClassArray);
	}
	
	public WebClientDetails(String url, HttpMethod method, Object requestBody,  Class<RequestClass> requestClass,  HttpHeaders headers, Long requestStartTime) throws Throwable
	{
		this.requestStartTime = requestStartTime;
		this.url          = url;
		this.method       = method;
		this.requestClass = requestClass;
		this.headers      = headers;
		
		//For a new request generate message id and replace inbound message id
		this.headers.remove("X-Message-Id");
		this.headers.add("X-Message-Id", generateGuid());
						
		if (requestBody != null)
		    this.requestBody = new ObjectMapper().readValue(String.valueOf(requestBody), requestClass);
		else
			this.requestBody = requestBody;
	}
	
	public String generateGuid()
	{
		return String.valueOf(UUID.randomUUID());
	}
}
