package net.bglgroup.spring.common.apiframework.model;

public class ErrorResponse 
{
	private String code;
	private String description;
	
	public ErrorResponse(String code, String description)
	{
		this.setCode(code);
		this.setDescription(description);
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
