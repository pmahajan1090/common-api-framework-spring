package net.bglgroup.spring.common.apiframework.requests;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;

import org.everit.json.schema.ValidationException;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import net.bglgroup.spring.common.apientry.enums.HeadersEnum;
import net.bglgroup.spring.common.apiframework.exceptions.BadRequestException;

import java.util.stream.Collectors;

public class ApiJsonRequest extends AbstractApiRequest
{
	private String contentType;	
	private JSONObject payload;
	
	public ApiJsonRequest(HttpServletRequest request, HeadersEnum[] expectedHeaders) throws BadRequestException 
	{
		super(request, expectedHeaders);	

		loadPayload(request);
	}
	
	private void loadPayload(HttpServletRequest request) throws BadRequestException
	{
	    contentType = request.getContentType();		
		
	    try 
		{
			String payloadString = request.getReader().lines().collect(Collectors.joining());
					
			if (payloadString != null && !payloadString.equals(""))
			{
				payload = new JSONObject(new JSONTokener(payloadString));				
			}
		}
		catch (ValidationException validationException)
		{
			throw new BadRequestException("Json Schema Validation for Request Body:" + validationException.getMessage());
		}
	    catch (JSONException jsonException)
	    {
	    	throw new BadRequestException("Json Format Invalid Payload:" + jsonException.getMessage());
	    }
	    catch (IOException ioException)
	    {
	    	throw new RuntimeException("Failed to read Payload:" + ioException.getMessage());
	    }
	}

	public Object getPayload() 
	{
		return payload;
	}
	
	public String getContentType() 
	{
		return contentType;
	}
}
