package net.bglgroup.spring.common.apiframework.webclientrequests;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.function.Consumer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientException;

import net.bglgroup.spring.common.apientry.loggers.Logger;

@Component
public class WebClientCalls 
{							
	@Autowired
	private WebClient webClient;
	
	@Autowired
	private Logger<WebClientDetails<?, ?>, String> webClientLogger;

		
	public ArrayList<Object> webClientParallelCalls(ArrayList<WebClientDetails<?,?>> webClientDetailsList) throws Throwable 
	{
		ExecutorService executorService = Executors.newFixedThreadPool(webClientDetailsList.size());
		
		var futures = executorService.invokeAll(createCallables(webClientDetailsList));
		var responseList = new ArrayList<Object>();
			
		try 
		{
			for(Future<Object> future : futures)
			{
				responseList.add(future.get());
			}
		}
		catch (Exception e)
		{
			//Re throw the original exception from the call so the correct http error is propagated.
			throw e.getCause();
		}
		finally 
		{
			executorService.shutdown();
		}
		
		return responseList; 
	} 
	
	public Set<Callable<Object>> createCallables(ArrayList<WebClientDetails<?,?>> webClientDetailsList) throws Throwable 
	{
		var callables = new HashSet<Callable<Object>>();

		for (WebClientDetails<?,?> webClientDetails : webClientDetailsList)
		{			
			callables.add(new Callable<Object>()
			{
				public Object call() throws Exception 
			    {
					return webClientCall(webClientDetails);
			    }
			});	
		}
		
		return callables;
	}
	
	public Object webClientCall(WebClientDetails<?,?> webClientDetails)
	{				
		webClientLogger.printStartLogStatement(webClientDetails);
		
		Object responseObject;
				
		try {
			switch (webClientDetails.getMethod()) 
					{
					    case GET -> responseObject = (webClientDetails.isArrayResponse()) ?
					    					webClient.get().uri(webClientDetails.getUrl())
					    								   .headers(getConsumerHeaders(webClientDetails.getHeaders()))
					    								   .retrieve()										  
					    								   .bodyToMono(webClientDetails.getResponseClassArray().get())
					    								   .block() :
					    					webClient.get().uri(webClientDetails.getUrl())
					    						   	       .headers(getConsumerHeaders(webClientDetails.getHeaders()))
					    						           .retrieve()										  
					    						           .bodyToMono(webClientDetails.getResponseClass().get())
					    						           .block();
					    case PATCH -> responseObject = webClient.patch().uri(webClientDetails.getUrl())
		    							               					.headers(getConsumerHeaders(webClientDetails.getHeaders()))
		    							               					.contentType(MediaType.APPLICATION_JSON)
		    							               					.bodyValue(webClientDetails.getRequestBody())
		    							               					.retrieve()										  
		    							               					.bodyToMono(webClientDetails.getResponseClass().get())
		    							               					.block();	
					    case POST -> responseObject = webClient.post().uri(webClientDetails.getUrl())
               														  .headers(getConsumerHeaders(webClientDetails.getHeaders()))
               														  .contentType(MediaType.APPLICATION_JSON)
               														  .bodyValue(webClientDetails.getRequestBody())
               														  .retrieve()										  
               														  .bodyToMono(webClientDetails.getResponseClass().get())
               														  .block();
					    //PUT/DELETE to be added when needed
					    default-> responseObject = null;
			        };
		}
		catch (WebClientException ex)
		{
			//ex.getStatusCode().name()
			webClientLogger.printEndLogStatement(webClientDetails, "Failure", System.currentTimeMillis());
			throw ex;
		}
		        
		webClientLogger.printEndLogStatement(webClientDetails, "Success", System.currentTimeMillis());
		        
		return responseObject;
		
	}
	
	
	private Consumer<HttpHeaders> getConsumerHeaders(HttpHeaders headers)
	{		
		Consumer<HttpHeaders> modifiedHeaders = it -> it.addAll(headers);
		
		return modifiedHeaders;
	}
}
