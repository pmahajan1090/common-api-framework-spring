package net.bglgroup.spring.common.apiframework.queuerequests;

import org.everit.json.schema.ValidationException;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.springframework.http.HttpHeaders;

import lombok.Getter;
import net.bglgroup.spring.common.apientry.enums.HeadersEnum;
import net.bglgroup.spring.common.apiframework.exceptions.BadRequestException;

@Getter
public class QueueJsonRequest implements QueueRequest
{
	private HttpHeaders headers = new HttpHeaders();
	private JSONObject  payload;
	
	public QueueJsonRequest(String queueMessage, HeadersEnum[] expectedHeaders) throws BadRequestException 
	{
		generateHeaders(expectedHeaders);
		loadPayload(queueMessage);
	}
	
	private void generateHeaders(HeadersEnum[] expectedHeaders)
	{	
		for (HeadersEnum expectedHeader : expectedHeaders)
		{
			String headerName  = expectedHeader.getName();
			headers.set(headerName, expectedHeader.getDefaultValue());
		}
	}
	
	private void loadPayload(String queueMessage) throws BadRequestException
	{		
	    try 
		{					
			if (queueMessage != null && !queueMessage.equals(""))
			{
				payload = new JSONObject(new JSONTokener(queueMessage));				
			}
		}
		catch (ValidationException validationException)
		{
			throw new BadRequestException("Json Schema Validation for Request Body:" + validationException.getMessage());
		}
	    catch (JSONException jsonException)
	    {
	    	throw new BadRequestException("Json Format Invalid Payload:" + jsonException.getMessage());
	    }
	}
}
