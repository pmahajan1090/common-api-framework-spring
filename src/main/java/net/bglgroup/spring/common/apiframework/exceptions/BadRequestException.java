package net.bglgroup.spring.common.apiframework.exceptions;

public class BadRequestException extends Exception
{
	private static final long serialVersionUID = 1L;
	
	private String message = null;

	public BadRequestException(String message)
	{
		this.message = message;
	}
	
	@Override
	public String getMessage()
	{
		return message;
	}	
}
