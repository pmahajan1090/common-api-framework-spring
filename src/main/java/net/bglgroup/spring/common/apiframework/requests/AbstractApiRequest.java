package net.bglgroup.spring.common.apiframework.requests;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpHeaders;

import lombok.Getter;
import net.bglgroup.spring.common.apientry.enums.HeadersEnum;

@Getter
public abstract class AbstractApiRequest implements ApiRequest
{
	private String method;
	private String url;
	private HttpHeaders headers = new HttpHeaders();
	private Map<String, Object> headersMap = new HashMap<String, Object>();
	private ArrayList<String>   includeQueryParams = new ArrayList<String>();
	private Map<String, String> standardQueryParams = new HashMap<String,String>();
		
	public AbstractApiRequest( HttpServletRequest request, HeadersEnum[] expectedHeaders)
	{
		method      = request.getMethod();
		url         = request.getRequestURI();
						
		extractHeaders(request, expectedHeaders);
		extractQueryParameters(request);
	}
	
	private void extractHeaders(HttpServletRequest request, HeadersEnum[] expectedHeaders)
	{	
		for (HeadersEnum expectedHeader : expectedHeaders)
		{
			String headerName  = expectedHeader.getName();
			Optional<String> headerValue = Optional.ofNullable(request.getHeader(headerName));
			
			//If no header is found check attributes for the desired header
			headerValue = (headerValue.isPresent()) ? headerValue : Optional.ofNullable((String)request.getAttribute(headerName));
			
			//Only add if header value was found
			if (headerValue.isPresent())
			{
				headersMap.put(headerName, headerValue.get());
			    headers.set(headerName, headerValue.get());
			}
		}
	}
	
	private void extractQueryParameters(HttpServletRequest request)
	{
		for (Map.Entry<String, String[]> entry : request.getParameterMap().entrySet()) 
		{
		    var key    = entry.getKey();
		    var values = entry.getValue();		    
		    
			for (int i=0; i < values.length; i++)
		    {
				if (key.equals("include"))
			    {
					includeQueryParams.addAll(Arrays.asList(values[i].replace(" ", "").split(",")));
			    }
				else 
				{					
					standardQueryParams.put(key, values[i]);
				}
		    }
		}
	}
	
    @Override	
	public String rebuildInludeString(List<String> requiredIncludes)
	{
		String includeString = "include=";
					
		for (String includeName : requiredIncludes) 
		{
			if (includeExists(includeName))
			{
			    includeString = includeString.concat(includeName + ",");
			}
		}
		
		return (hasIncludes()) ? includeString.substring(0, includeString.length()-1) : "";
	}
	
	
	@Override
	public boolean actionsRequested()
	{
		return includeExists("actions");
	}
	
	@Override
	public boolean hasIncludes()
	{
		return (includeQueryParams.size() > 0);
	}
	
	@Override
	public boolean isBasketContext()
	{
		return (this.getHeader("Basket-Id") != null && (!this.getHeader("Basket-Id").isEmpty()));
	}	
	
	@Override
	public String getHeader(String headerName) 
	{
		return (String) headersMap.get(headerName);
	}

	@Override
	public boolean includeExists(String includeName) 
	{
		return includeQueryParams.contains(includeName);
	}

	@Override
	public String getQueryParemeter(String queryParameterName) 
	{
		return standardQueryParams.get(queryParameterName);
	}
	
	@Override
	public abstract Object getPayload();

	@Override
	public abstract String getContentType();
}
