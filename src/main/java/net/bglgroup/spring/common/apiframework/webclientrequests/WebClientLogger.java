package net.bglgroup.spring.common.apiframework.webclientrequests;

import java.text.MessageFormat;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import net.bglgroup.spring.common.apientry.enums.StandardHeadersEnum;
import net.bglgroup.spring.common.apientry.loggers.AbstractLogger;

public class WebClientLogger<Request, Response> extends AbstractLogger<Request, Response> 
{
	private final String LOG_START_MESSAGE_TEMPLATE = "IO={7}, HM={0}, URI={1}, IT={2}, IR={3}, IM={4}, IB={5}, E={6}";
	private final String LOG_END_MESSAGE_TEMPLATE = "IO={9}, HM={0}, URI={1}, IT={2}, IR={3}, IM={4}, IB={5}, E={6}, RC={7}, RT={8,number,############}";

	protected Log logManager = LogFactory.getLog(getClass().getSimpleName());

	public WebClientLogger(String apiLayer) 
	{
		this.apiLayer = apiLayer;
	}

	@Override
	protected String composeStartLogStatement(Request request) 
	{        
		var webClientDetails = (WebClientDetails<?,?>) request;
		
		return MessageFormat.format(LOG_START_MESSAGE_TEMPLATE, getAttribute(webClientDetails.getMethod().name()),
				getAttribute(webClientDetails.getUrl()),
						getAttribute(webClientDetails.getHeaders().getFirst(StandardHeadersEnum.TRACE_ID.getName())),
						getAttribute(webClientDetails.getHeaders().getFirst(StandardHeadersEnum.REQUEST_ID.getName())),
						getAttribute(webClientDetails.getHeaders().getFirst(StandardHeadersEnum.MESSAGE_ID.getName())),
						getAttribute(webClientDetails.getHeaders().getFirst(StandardHeadersEnum.BASKET_ID.getName())), apiLayer, "Inbound");
	}

	@Override
	protected String composeEndLogStatement(Request request, Response response, Long endTime) 
	{
		var webClientDetails = (WebClientDetails<?,?>) request;
		String status = (String) response;
		
		return MessageFormat.format(LOG_END_MESSAGE_TEMPLATE, getAttribute(webClientDetails.getMethod().name()),
				getAttribute(webClientDetails.getUrl()),
				getAttribute(webClientDetails.getHeaders().getFirst(StandardHeadersEnum.TRACE_ID.getName())),
				getAttribute(webClientDetails.getHeaders().getFirst(StandardHeadersEnum.REQUEST_ID.getName())),
				getAttribute(webClientDetails.getHeaders().getFirst(StandardHeadersEnum.MESSAGE_ID.getName())),
				getAttribute(webClientDetails.getHeaders().getFirst(StandardHeadersEnum.BASKET_ID.getName())), apiLayer,
				status, 
				(endTime - webClientDetails.getRequestStartTime()), "Outbound");
	}

}
