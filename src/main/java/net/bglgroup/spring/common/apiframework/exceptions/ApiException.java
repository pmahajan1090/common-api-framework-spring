package net.bglgroup.spring.common.apiframework.exceptions;

import org.springframework.http.HttpStatus;

import net.bglgroup.spring.common.apiframework.model.ErrorMessages;

public class ApiException extends Exception 
{
	private static final long serialVersionUID = 1L;
	
	private HttpStatus httpStatus;
	private ErrorMessages errorMessages = null;
	private String errorMessage = null;

	public ApiException(HttpStatus httpStatus, String errorMessage)
	{
		this.errorMessage = errorMessage;
		this.httpStatus = httpStatus;
	}
	
	public ApiException(HttpStatus httpStatus, String errorMessage, ErrorMessages errorMessages)
	{
		this(httpStatus, errorMessage);
		this.errorMessages = errorMessages;		
	}
	
	@Override
	public String getMessage()
	{
		return errorMessage;
	}		
	
	public ErrorMessages getErrorMessages()
	{
		return errorMessages;
	}
		
	public HttpStatus getHttpStatus()
	{
		return httpStatus;
	}
}
