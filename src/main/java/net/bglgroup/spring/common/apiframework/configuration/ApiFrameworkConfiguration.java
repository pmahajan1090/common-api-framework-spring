package net.bglgroup.spring.common.apiframework.configuration;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.reactive.function.client.WebClient;

import net.bglgroup.spring.common.apientry.enums.ApiRequestEnum;
import net.bglgroup.spring.common.apientry.loggers.Logger;
import net.bglgroup.spring.common.apiframework.webclientrequests.WebClientDetails;
import net.bglgroup.spring.common.apiframework.webclientrequests.WebClientLogger;


@Configuration
@ServletComponentScan(basePackages = "net.bglgroup.spring.common.apientry")
@ComponentScan(basePackages = {"net.bglgroup.spring.common.apientry"})
public class ApiFrameworkConfiguration 
{    		 
	 @Bean
     public RestTemplate restTemplate() 
     {
     	 return new RestTemplateBuilder().build();
     }
	 
	 @Bean
     public WebClient webClient() 
     {
     	 return  WebClient.create();
     }

	@Bean
	public Logger<WebClientDetails<?, ?>, String> webClientLogger() 
	{
		 return new WebClientLogger<WebClientDetails<?, ?>, String>(ApiRequestEnum.SYSTEM.name());
	}
}
