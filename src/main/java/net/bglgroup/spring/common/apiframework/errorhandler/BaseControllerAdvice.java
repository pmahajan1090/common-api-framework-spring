package net.bglgroup.spring.common.apiframework.errorhandler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;
import org.json.JSONTokener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.UnknownHttpStatusCodeException;
import org.springframework.web.reactive.function.client.WebClientRequestException;
import org.springframework.web.reactive.function.client.WebClientResponseException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.atlassian.oai.validator.springmvc.InvalidRequestException;
import com.atlassian.oai.validator.springmvc.InvalidResponseException;

import net.bglgroup.spring.common.apiframework.exceptions.ApiException;
import net.bglgroup.spring.common.apiframework.exceptions.BadRequestException;
import net.bglgroup.spring.common.apientry.loggers.Logger;

@ControllerAdvice
public class BaseControllerAdvice extends ResponseEntityExceptionHandler
{
	    
	@Autowired
	protected Logger<HttpServletRequest, HttpServletResponse> httpLogger;
		
	
	/*** Web Client Rest Call ***/
	@ResponseBody
	@ExceptionHandler(WebClientRequestException.class)
	ResponseEntity<String> restExceptionHandler(WebClientRequestException ex, HttpServletRequest request, HttpServletResponse response)
	{ 				
		response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
		httpLogger.printEndLogStatement(ex.getMessage(), request, response, System.currentTimeMillis());
					
		return new ResponseEntity<String>( ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	@ResponseBody
	@ExceptionHandler(WebClientResponseException.class)
	ResponseEntity<Object> restExceptionHandler(WebClientResponseException ex, HttpServletRequest request, HttpServletResponse response)
	{ 				
		response.setStatus(ex.getStatusCode().value());
		httpLogger.printEndLogStatement(ex.getMessage(), request, response, System.currentTimeMillis());
							
		HttpHeaders headers = new HttpHeaders();
	    headers.setContentType(MediaType.APPLICATION_JSON);
	    
		return new ResponseEntity<Object>(ex.getResponseBodyAsString(), headers, ex.getStatusCode());
	}
	
	/*** Rest Template ***/
	@ResponseBody
	@ExceptionHandler(HttpClientErrorException.class)
	//@ResponseStatus(HttpStatus.4xx)
	ResponseEntity<String> restExceptionHandler(HttpClientErrorException ex, HttpServletRequest request, HttpServletResponse response)
	{ 				
		response.setStatus(ex.getStatusCode().value());
		httpLogger.printEndLogStatement(ex.getMessage(), request, response, System.currentTimeMillis());
			
		return new ResponseEntity<String>( ex.getResponseBodyAsString(), ex.getStatusCode());
	}
	
	@ResponseBody
	@ExceptionHandler(HttpServerErrorException.class)
	//@ResponseStatus(HttpStatus.5xx)
	ResponseEntity<String> restExceptionHandler(HttpServerErrorException ex, HttpServletRequest request, HttpServletResponse response)
	{ 				
		response.setStatus(ex.getStatusCode().value());
		httpLogger.printEndLogStatement(ex.getMessage(), request, response, System.currentTimeMillis());
			
		return new ResponseEntity<String>( ex.getResponseBodyAsString(), ex.getStatusCode());
	}
	
	@ResponseBody
	@ExceptionHandler(UnknownHttpStatusCodeException.class)
	//@ResponseStatus(HttpStatus.xxx)
	ResponseEntity<String> restExceptionHandler(UnknownHttpStatusCodeException ex, HttpServletRequest request, HttpServletResponse response)
	{ 				
		response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
		httpLogger.printEndLogStatement(ex.getMessage(), request, response, System.currentTimeMillis());
			
		return new ResponseEntity<String>( ex.getResponseBodyAsString(), HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	/*** Open API Validation Exception ***/	
	@ResponseBody
	@ExceptionHandler(InvalidRequestException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	ResponseEntity<String> apiBadRequestHandler(InvalidRequestException ex, HttpServletRequest request, HttpServletResponse response) 
	{	 
		JSONObject errorMessages = new JSONObject(new JSONTokener(ex.getMessage()));
		JSONObject errorMessage = errorMessages.getJSONArray("messages").getJSONObject(0);
		
		String errorMessageString = errorMessage.getString("message");
		
		response.setStatus(HttpStatus.BAD_REQUEST.value());
		httpLogger.printEndLogStatement(errorMessageString, request, response, System.currentTimeMillis());		
		
				
		return new ResponseEntity<String>(errorMessageString, HttpStatus.BAD_REQUEST);
	}
	
	@ResponseBody
	@ExceptionHandler(InvalidResponseException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	ResponseEntity<String> apiBadRequestHandler(InvalidResponseException ex, HttpServletRequest request, HttpServletResponse response) 
	{	 
		JSONObject errorMessages = new JSONObject(new JSONTokener(ex.getMessage()));
		JSONObject errorMessage = errorMessages.getJSONArray("messages").getJSONObject(0);
		
		String errorMessageString = errorMessage.getString("message");
		
		response.setStatus(HttpStatus.BAD_REQUEST.value());
		httpLogger.printEndLogStatement(errorMessageString, request, response, System.currentTimeMillis());		
		
		return new ResponseEntity<String>(errorMessageString, HttpStatus.BAD_REQUEST);
	}
	
	/*** Java Exception ***/
	@ResponseBody
	@ExceptionHandler(Throwable.class)
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	ResponseEntity<String> exceptionHandler(Exception ex, HttpServletRequest request, HttpServletResponse response) 
	{ 		
		response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());		
		httpLogger.printEndLogStatement(ex.getMessage(), request, response, System.currentTimeMillis());
		
		// Should include trace id
		ex.printStackTrace();
				
		return new ResponseEntity<String>( ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
	}
		
	/*** API Exceptions ***/
	@ResponseBody
	@ExceptionHandler(ApiException.class)
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	ResponseEntity<String> apiExceptionHandler(Exception ex, HttpServletRequest request, HttpServletResponse response) 
	{ 		
		response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());		
		httpLogger.printEndLogStatement(ex.getMessage(), request, response, System.currentTimeMillis());
				
		return new ResponseEntity<String>( ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
	}	
		
	@ResponseBody
	@ExceptionHandler(BadRequestException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	ResponseEntity<String> apiBadRequestHandler(BadRequestException ex, HttpServletRequest request, HttpServletResponse response) 
	{	 
		response.setStatus(HttpStatus.BAD_REQUEST.value());
		httpLogger.printEndLogStatement(ex.getMessage(), request, response, System.currentTimeMillis());		
		
		return new ResponseEntity<String>(ex.getMessage(), HttpStatus.BAD_REQUEST);
	}
}
