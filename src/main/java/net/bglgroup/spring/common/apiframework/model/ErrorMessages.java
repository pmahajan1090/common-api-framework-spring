package net.bglgroup.spring.common.apiframework.model;

import java.util.ArrayList;

public class ErrorMessages 
{	
    ArrayList<ErrorResponse> messages;
    
    public ErrorMessages(ArrayList<ErrorResponse> messages)
    {
    	this.messages = messages;
    }
    
    public ArrayList<ErrorResponse> getMessages()
    {
    	return messages;
    }
}
