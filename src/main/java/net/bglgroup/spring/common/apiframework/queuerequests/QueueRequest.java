package net.bglgroup.spring.common.apiframework.queuerequests;

import org.springframework.http.HttpHeaders;

public interface QueueRequest 
{
    public HttpHeaders getHeaders();    
    public Object getPayload();
}
