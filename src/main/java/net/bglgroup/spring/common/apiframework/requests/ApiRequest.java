package net.bglgroup.spring.common.apiframework.requests;

import java.util.List;
import java.util.Map;

import org.springframework.http.HttpHeaders;

public interface ApiRequest 
{
    public String getMethod();
    public String getUrl();
    
    public HttpHeaders getHeaders();
    public String getHeader(String headerName);
    public Map<String, Object> getHeadersMap();
    
    public boolean isBasketContext();
    
    public boolean includeExists(String includeName);
    public String rebuildInludeString(List<String> requiredIncludes);
    public boolean hasIncludes();
    public boolean actionsRequested();
    
    public String getQueryParemeter(String queryParameterName);
      
    public abstract Object getPayload();
	public abstract String getContentType();
}
